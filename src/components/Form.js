import React, {useState} from "react";
import Input from './Input';
import Button from './Button';
import Request from '../services/Request';

let typingTimer;

const Form = (props) => {
    const [value, setValue] = useState('');

    const handleClick = async (event) => {
        event.preventDefault();
        props.loading(true);
        let data = await Request('search/movie', value);
        props.loading(false);
        props.onChange([data]);
    }

    const handleChange = async (event) => {
        const {value} = event.target;
        setValue(value);

        clearTimeout(typingTimer);

        if (value && value !== "" && value.length > 2) {
            typingTimer = window.setTimeout(async () => {
                try {
                    props.loading(true);
                    let data = await Request('search/movie', value);
                    props.onChange([data]);
                    props.loading(false);

                } catch (err) {
                    console.error(err);
                }
            }, 300);
        }
    }

    return(
        <div className="form">
            <form>
                <h2>{props.title}</h2>
                <Input value={value} onChange={handleChange} />
                <Button value={props.value} onClick={handleClick} />
            </form>
        </div>
    );
}

export default Form;
