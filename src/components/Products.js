import React from "react";

const Products = (props) => {
    const productList = props.productList.map(function(product, index) {
        return (
            <li key={product.id} className="product" data-name={product.id}>
                <img src={product.poster_path} alt="" />
                <h3>{product.original_title}</h3>
                <p><strong>Release date:</strong> {product.release_date}</p>
                <p>{product.overview}</p>
                <p><strong>Rating:</strong> {product.vote_average}</p>
            </li>
        );
    });

    return(<ul className="products">{ productList }</ul>);
}

export default Products;
