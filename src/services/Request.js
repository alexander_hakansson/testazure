const Request = (endpoint, value, page= 0) => {
    return new Promise((resolve, reject) => {
        if (value && value.length < 3) {
            resolve('');
        }

        const apiUrl = process.env.REACT_APP_API_URL;
        const apiKey = process.env.REACT_APP_API_KEY;

        let requestUrl = `${apiUrl}${endpoint}?api_key=${apiKey}`;
        if (value) {
            requestUrl = requestUrl + '&query=' + value;
        }
        else {
            requestUrl = apiUrl + endpoint + '?api_key=' + apiKey + '&language=en-US&page=' + page
        }

        fetch(requestUrl)
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                resolve(data);
            })
            .catch((err) => {
                reject(err);
            });
    });
};

export default Request;
