import strings from './strings.json'

class I18n {
    constructor() {
        this.strings = strings;
    }
    get get() {
        return this.strings;
    }
}

export default new I18n();
