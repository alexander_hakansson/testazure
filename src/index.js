import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';

import Button from './components/Button';
import Form from './components/Form';
import Products from './components/Products';
import Request from './services/Request';
import I18n from "./services/I18n";
import './normalize.css';
import './index.css';

const App = () => {
    const [productList, setProductList] = useState([]);
    const [loading, setLoading] = useState(false);

    const handleClick = async (event) => {
        event.preventDefault();
        setLoading(true);

        let data = [];
        for (let page = 1; page <= 5; page++) {
            let dataPage = await Request('movie/popular', false, page);
            if (dataPage) {
                data.push(dataPage);
            }
        }

        if (data.length > 0) {
            handleProductList(data);
        }
        setLoading(false);
    }

    const handleProductList = (data) => {

        let products = [];
        Object.entries(data).forEach(([key, dataEntry]) => {

            if (dataEntry && dataEntry.results) {
                Object.entries(dataEntry.results).forEach(([key, value]) => {
                    let product = {};

                    Object.entries(value).forEach(([key, value]) => {
                        if(key === 'poster_path') {
                            value = 'https://image.tmdb.org/t/p/w500/' + value;
                        }
                        product[key] = value;
                    });
                    products.push(product);
                });
            }
        });

        setProductList(products);
    }

    const handleLoader = (loading) => {
        setLoading(loading);
    }

    return (
        <div className="app">
            <h1>Lek react-app</h1>

            <Button value={I18n.get.labels.fetch} onClick={handleClick} />
            <Form value={I18n.get.labels.search} loading={handleLoader} onChange={handleProductList} title={I18n.get.labels.searchTitle} />
            {loading && (<div className="loader"></div>)}
            <Products productList={productList} />
        </div>
    );
}


ReactDOM.render(
    <App />,
    document.getElementById('root')
);
